#!/usr/bin/env bash

wget 'https://www.genenames.org/cgi-bin/download/custom?col=gd_hgnc_id&col=md_refseq_id&status=Approved&status=Entry%20Withdrawn&hgnc_dbtag=on&order_by=gd_app_sym_sort&format=text&submit=submit' -O genenames-refseq.tsv
